///-------------------------------------------------------------------------------------------------
///
/// \file StimulationInlet.cs
/// \brief Implementation for an Inlet receiving Stimulations (int) from OpenViBE.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 24/03/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
///
///-------------------------------------------------------------------------------------------------

using LSL4Unity.OV;
using UnityEngine;

namespace OVGames.HelloBidirectionnal
{
/// <summary> Implementation for an Inlet receiving Stimulations (int) from OpenViBE. </summary>
/// <seealso cref="OVIntInlet" />
public class StimulationInlet : OVIntInlet
{
	/// <summary> Member that contains the last sample. </summary>
	/// <value> The last sample. </value>
	public int[] LastSample { get; set; }

	/// <summary> Process when samples are available. </summary>
	/// <param name="input"> The Incomming Sample. </param>
	/// <param name="time"> The current Time. </param>
	protected override void Process(int[] input, double time)
	{
		LastSample = input;
		Debug.Log($"Got {input.Length} ints at {time}");
	}
}
}
