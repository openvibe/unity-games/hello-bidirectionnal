///-------------------------------------------------------------------------------------------------
///
/// \file FloatInlet.cs
/// \brief Implementation for an Inlet receiving Float values for OpenViBE Link.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 24/03/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
///
///-------------------------------------------------------------------------------------------------

using LSL4Unity.OV;

namespace OVGames.HelloBidirectionnal
{
/// <summary> An example of implementation for an Inlet receiving Float values for OpenViBE Link. </summary>
/// <seealso cref="OVFloatInlet" />
public class FloatInlet : OVFloatInlet
{
	/// <summary> Member that contains the last sample. </summary>
	/// <value> The last sample. </value>
	public float[] LastSample { get; private set; }

	/// <summary> Process when samples are available. </summary>
	/// <param name="input"> The Incomming Sample. </param>
	/// <param name="time"> The current Time. </param>
	protected override void Process(float[] input, double time) { LastSample = input; }
}
}
