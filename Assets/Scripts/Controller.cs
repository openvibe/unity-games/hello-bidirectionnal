///-------------------------------------------------------------------------------------------------
///
/// \file Controller.cs
/// \brief Main Controller to manage the cube.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 24/03/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
///
///-------------------------------------------------------------------------------------------------

using System;
using System.Linq;
using LSL4Unity;
using LSL4Unity.OV;
using UnityEngine;

namespace OVGames.HelloBidirectionnal
{
/// <summary> Controller is used to manage the cube. </summary>
/// <seealso cref="UnityEngine.MonoBehaviour" />
public class Controller : MonoBehaviour
{
	// Color Management
	private static readonly int      ColNameId = Shader.PropertyToID("_Color");
	private readonly        Color[]  colors    = { Color.blue, Color.cyan, Color.green, Color.yellow, Color.red, Color.magenta };
	private                 int      colPos    = 0;
	private                 Material material;

	// Settings
	[SerializeField] private Vector3          rotationAxis     = new Vector3(10.0F, 10.0F, 10.0F);
	[SerializeField] private float            scale            = 1;
	[SerializeField] private string           signalOutletName = "ovInSignal";
	[SerializeField] private string           markerOutletName = "ovInMarkers";
	[SerializeField] private FloatInlet       signalInlet;
	[SerializeField] private StimulationInlet stimInlet;

	// Outlet Variables
	private       liblsl.StreamOutlet outletSignal, outletMarker;
	private const int                 STIMULATION = (int) Stimulations.GDF_BEEP;
	private       double              startTime;

	/// <summary> Start is called before the first frame update. </summary>
	private void Start()
	{
		material = GetComponent<Renderer>().material;

		// Create Streams
		liblsl.StreamInfo info = new liblsl.StreamInfo(signalOutletName, "signal", 1, liblsl.IRREGULAR_RATE, liblsl.channel_format_t.cf_float32, "signal");
		outletSignal = new liblsl.StreamOutlet(info);
		Debug.Log($"Creating Stream : Name = {info.Name()}, Type = {info.Type()}, Channel Count = {info.ChannelCount()}, Format = {info.ChannelFormat()}");
		info         = new liblsl.StreamInfo(markerOutletName, "Marker", 1, liblsl.IRREGULAR_RATE, liblsl.channel_format_t.cf_int32, "stimulation");
		outletMarker = new liblsl.StreamOutlet(info);
		Debug.Log($"Creating Stream : Name = {info.Name()}, Type = {info.Type()}, Channel Count = {info.ChannelCount()}, Format = {info.ChannelFormat()}");
		startTime = liblsl.LocalClock(); // Initialize Time
	}

	///<summary> Update is called once per frame. </summary>
	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Debug.Log("Quit Game...");
			Application.Quit();
		}

		// Check Stream
		if (!signalInlet.IsSolved()) { signalInlet.ResolveStream(); }
		if (!stimInlet.IsSolved()) { stimInlet.ResolveStream(); }

		// Rotate the cube
		transform.Rotate(rotationAxis * Time.deltaTime);

		// Change Scale
		if (signalInlet.LastSample != null && signalInlet.LastSample.Length > 0)
		{
			var value = Math.Abs(signalInlet.LastSample[0]) * scale;
			if (value > 1e-6) { transform.localScale = new Vector3(value, value, value); }
			outletSignal.PushSample(new[] { value }, liblsl.LocalClock() - startTime); // Send samples with unity time
		}

		// Change Color
		if (stimInlet.LastSample != null && stimInlet.LastSample.Length > 0 && stimInlet.LastSample[0] == (int) Stimulations.LABEL_00)
		{
			colPos++;
			if (colPos >= colors.Length) { colPos = 0; }

			Debug.Log($"Stimulation received : {Stimulations.LABEL_00} {nameof(Stimulations.LABEL_00)}, new Pos : {colPos}, new Color : {colors[colPos]}");
			// The Stimulation stay in the variable if we don't remove the first
			stimInlet.LastSample = stimInlet.LastSample.Skip(1).ToArray();
			material.SetColor(ColNameId, colors[colPos]);
			outletMarker.PushSample(new[] { STIMULATION }, liblsl.LocalClock() - startTime); // Send Stimulation with unity time
		}
	}

	// <summary> Fixedupdate is called once per physics framerate. </summary>
	// private void FixedUpdate() { }
}
}
